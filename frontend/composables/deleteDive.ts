import { deleteDoc, doc } from "firebase/firestore";

export default async function (id: string) {
    const db = useFirestore()

    await deleteDoc(doc(db, "dives", id));
}
