import { collection, getDocs, orderBy, query, where } from "firebase/firestore";

export default async function () {
    const db = useFirestore()
    const user = await getCurrentUser()
    const dives = {}

    const q = query(collection(db, "dives"), where("user", "==", user.uid), orderBy("diveNumber", "desc"));

    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc) => {
        dives[doc.id] = doc.data()
    });
    return dives
}
