export default function(value: unknown) {
    if (value === null) {
        return 0
    }
    return value
}
