import { collection, getDocs } from "firebase/firestore";

export default async function () {
    const db = useFirestore()
    const locations = {}

    const querySnapshot = await getDocs(collection(db, "locations"));
    querySnapshot.forEach((doc) => {
        locations[doc.id] = doc.data()
    });
    return locations
}
