export default defineEventHandler(async (event) => {
    const req = getQuery(event)

    const runtimeConfig = useRuntimeConfig()

    const url = new URL("https://api.weatherapi.com/v1/marine.json")
    url.searchParams.append("key", runtimeConfig.weatherApiKey)
    url.searchParams.append("q", `${req.lat},${req.long}`)
    url.searchParams.append("days", "5")

    const data: object = await $fetch(url.href)

    return data.forecast.forecastday
  }
)
