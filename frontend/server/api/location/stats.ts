import { initializeApp } from 'firebase-admin/app';
import { AggregateField, getFirestore } from 'firebase-admin/firestore';

initializeApp();
const db = getFirestore();

export default defineEventHandler(async (event) => {
    const req = getQuery(event)

    const q = db.collection('dives')
    .where('location', '==', req.location);

    const countSnapshot = await q.count().get();

    const averageSnapshot = await q.aggregate({
      averageTemperature: AggregateField.average('temperature'),
      averageDepth: AggregateField.average('depth'),
      averageDuration: AggregateField.average('duration'),
    }).get();

    return {
      count: countSnapshot.data().count,
      averageTemperature: averageSnapshot.data().averageTemperature,
      averageDepth: averageSnapshot.data().averageDepth,
      averageDuration: averageSnapshot.data().averageDuration
    }
  })
