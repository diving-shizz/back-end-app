// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: '2024-11-01',
  devtools: { enabled: true },
  modules: ['@nuxtjs/tailwindcss', 'nuxt-vuefire', '@nuxt/eslint', '@nuxt/icon'],
  css: ['~/assets/css/main.css'],
  ssr: false,
  runtimeConfig: {
    weatherApiKey: '',
  },
  vuefire: {
    auth: {
      enabled: true
    },
    config: {
      apiKey: 'AIzaSyCjyWLLa4TA8XSI7FgagvZwBAIIKBsYq94',
      authDomain: 'divingshizz.firebaseapp.com',
      projectId: 'divingshizz',
      appId: '1:947317020116:web:e33d02837d25330cea7241'
    },
  },
  nitro: {
    firebase: {
      gen: 2
    }
  }
})
